#!/usr/bin/perl

=head1 NAME

  lc1.pl

=head1 USAGE

  ./lc1.pl &

=head1 DESCRIPTION

Upon start, searches for old LaTeX to PDF conversion jobs and perfoms
them.  Then the script monitors the current directory for new conversion
jobs and executes them.

=cut

use strict;

use Filesys::Notify::Simple;
use File::Find;

my $work_dir= '.';

my @old_jobs=();

check_old_jobs();
watch();

exit (0);

sub watch
{
  my $watcher = Filesys::Notify::Simple->new([ $work_dir ]);

  while (1)
  {
    print __LINE__, " watching ...\n";
    $watcher->wait(sub {
       for my $event (@_) {
            my $path= $event->{path}; # full path of the file updated
            print __LINE__, " path=[$path]\n";

            if ($path =~ m#^.*/([\w\d]+)/Tex/([\w\d]+\.tex)$#)
            {
              my ($job, $tex_file)= ($1, $2);
              print __LINE__, " job=[$job] tex_file=[$tex_file]\n";
              latex2pdf ($job, $tex_file);
            }
            elsif ($path =~ m#/var/www/html/dlbt/DLBTUploads/TeiConverter/lc1.pl$#)
            {
              print "restarting\n";
              exec ($0);
            }
          }
      });
  }
}

sub check_tex_file
{
  my $path= $File::Find::name;
  # print __LINE__, " path=[$path]\n";

  if ($path =~ m#^.*/([\w\d]+)/Tex/([\w\d]+\.tex)$#)
  {
    my ($job, $tex_file)= ($1, $2);

    my $pdf_file= join ('/', $job, 'Pdf', "$job.pdf");
    push (@old_jobs, [$job, $tex_file, $pdf_file]);
  }

  0;
}

sub check_old_jobs
{
  find(\&check_tex_file, $work_dir);
  # find( { wanted => \&check_tex_file }, $work_dir);

  foreach my $item (@old_jobs)
  {
    my ($job, $tex_file, $pdf_file)= @$item;
    if (-f $pdf_file)
    {
      print __LINE__, " job=[$job] pdf_file=[$pdf_file] already exists, skipping;\n";
    }
    else
    {
      # print __LINE__, " job=[$job] tex_file=[$tex_file]\n";
      latex2pdf ($job, $tex_file);
    }
  }
}

sub latex2pdf
{
  my $jobname= shift;
  my $tex_file= shift;

  my $tex_path= "$jobname/Tex/$tex_file";
  my $file_info= `/usr/bin/file $tex_path 2>&1`;
  chop ($file_info);
  # print __LINE__, " file_info: [$tex_path] [$file_info]\n";
  unless ($file_info =~ m#LaTeX 2e document#)
  {
    print __LINE__, " not a LaTeX file, ignored: [$file_info]\n";
    return undef;
  }
  
  my $pdf_dir= "$jobname/Pdf"; 
  unless (-d $pdf_dir)
  {
    print __LINE__, " creating pdf_dir=[$pdf_dir]\n";
    system ('mkdir', $pdf_dir);
  }

  # pdflatex -interaction=nonstopmode -jobname PS293242 -output-directory PS293242/Pdf/ PS293242/Tex/document.tex
  my @convert= (qw(/usr/bin/pdflatex -interaction=nonstopmode -jobname), $jobname, '-output-directory', $pdf_dir, $tex_path);

  my $media_dir= "$jobname/Tex/media";
  if (-d $media_dir)
  {
    print __LINE__, " creating symlink media to [$media_dir]\n";
    system ('ln', '-s', $media_dir, 'media')
  }

  print "converting: [", join (' ', @convert), "]\n";
  sleep (5); # wait some time to avoid possible race conditions
  system (@convert);

  if (-d $media_dir)
  {
    print __LINE__, " removing symlink to [$media_dir]\n";
    unlink ('media')
  }
}

__END__

=head1 AUTHOR

  Gerhard Gonter <ggonter@cpan.org>

